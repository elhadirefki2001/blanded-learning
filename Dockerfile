FROM openjdk:17-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven executable JAR file and the POM file into the container
COPY target/ensaj-0.0.1-SNAPSHOT.jar app.jar
COPY pom.xml pom.xml

# Expose the port that the application will run on
EXPOSE 8082

# Specify the command to run on container startup
CMD ["java", "-jar", "app.jar"]
