package ensaj.planning.services;

import ensaj.planning.entities.AffectationModuleGroupeTeacher;
import ensaj.planning.entities.Session;

public interface ISession {
    Session save(Session session);
}
